import "./App.css";
import "./dateselector.css";
import DateSelected from "./components/DateSelected";
import ExpenseInput from "./components/ExpenseInput";
import ExpensesContainer from "./components/ExpensesContainer";
import { useEffect, useState } from "react";
// import { nanoid } from 'nanoid';
import Draggable from "react-draggable";
import DeleteConfirmPopup from "./components/DeleteConfirmPopup";
import axios from "axios";
import { createXandY } from "./components/functions/createXandY";
import { Graph } from "./components/Graph";

export default function App() {
  const [inExpenseMenu, setInExpenseMenu] = useState(false);
  const [dateSelected, setDateSelected] = useState("");

  // Expense name is set for showing it in the delete confirm popup, and for uploading expense name
  const [expenseName, setExpenseName] = useState("");
  const [categoryName, setCategoryName] = useState("");

  // Category chosen for post request
  const [categoryChosen, setCategoryChosen] = useState("");

  // Pricing
  const [expenseAmount, setExpenseAmount] = useState(0);

  // Actual content that gets rendered
  const [expensesContent, setExpensesContent] = useState([]);
  const [categories, setCategories] = useState([]);

  // States for triggering all popups
  const [deleteConfirmIsTriggered, setDeleteConfirmIsTriggered] =
    useState(false);
  const [inputIsTriggered, setInputIsTriggered] = useState(false);
  const [categoryInputIsTriggered, setCategoryInputIsTriggered] =
    useState(false);

  const [idAndNameToBeDeleted, setIdAndNameToBeDeleted] = useState({});

  const [totalSpendingState, setTotalSpendingState] = useState(0);
  const [xAxisAndYAxis, setXAndYAxis] = useState([]);

  function apiGet() {
    axios
      .get("http://localhost:5000/expenses")
      .then((res) => {
        setExpensesContent(res.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  useEffect(() => {
    // Creating an array of objects with date and price props
    setXAndYAxis(createXandY(expensesContent));

    // Calculating the total amount spent all time
    let totalSpending;
    totalSpending = expensesContent.map((expense) => {
      return expense.price;
    });

    let sum = 0;

    for (let i = 0; i < totalSpending.length; i++) {
      sum = sum + parseInt(totalSpending[i]);
    }
    totalSpending = sum;

    setTotalSpendingState(totalSpending);
  }, [expensesContent]);

  function apiGetCategories() {
    axios
      .get("http://localhost:5000/categories")
      .then((res) => {
        setCategories(res.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  useEffect(() => {
    apiGet();
  }, [inputIsTriggered, deleteConfirmIsTriggered]);

  useEffect(() => {
    apiGetCategories();
  }, [categoryName]);

  function postExpense() {
    if (expenseName.trim().length !== 0 && expenseAmount !== 0) {
      // Sets category as "Other" when no category is chosen
      let category = categoryChosen;
      if (categoryChosen === "") {
        category = "Other";
      }

      axios
        .post("http://localhost:5000/expenses", {
          name: expenseName,
          price: expenseAmount,
          category: category,
          date: dateSelected,
        })
        .then((res) => console.log(res))
        .then(setInputIsTriggered(false))
        .catch((err) => console.log(err));

      setExpenseName("");
      setExpenseAmount(0);
      setCategoryChosen("");
    }
  }

  function postCategory() {
    if (categoryName.trim().length !== 0) {
      axios
        .post("http://localhost:5000/categories", {
          categoryName: categoryName,
        })
        .then((res) => console.log(res))
        .then(setCategoryInputIsTriggered(false))
        .catch((err) => console.log(err));
      setCategoryName("");
      apiGet();
    }
  }

  function triggerDeleteConfirmPopup(id, name) {
    setDeleteConfirmIsTriggered(true);
    setIdAndNameToBeDeleted({ id, name });
  }

  function deleteExpense(id) {
    axios
      .delete(`http://localhost:5000/expenses/${id}`)
      .then((res) => console.log(res))
      .then(setDeleteConfirmIsTriggered(false))
      .catch((err) => console.log(err));

    //Maybe setTimeOut?
    apiGet();
  }

  function handleAddExpense() {
    setInputIsTriggered(!inputIsTriggered);
  }

  function handleAddCategory() {
    setCategoryInputIsTriggered(!categoryInputIsTriggered);
  }

  function changeToExpensesMenu() {
    setInExpenseMenu(true);
  }

  function changeToDateSelectorMenu() {
    setInExpenseMenu(false);
    setDateSelected("");
  }

  return (
    <div>
      {
        // Menu for viewing expenses on a particular day
        inExpenseMenu ? (
          <div className="totalContainer">
            <DateSelected date={dateSelected} />
            <button
              className="back-to-dateselector-btn"
              onClick={changeToDateSelectorMenu}
            >
              Back to date selector
            </button>
            <button className="add-expense-btn" onClick={handleAddExpense}>
              Add new expense
            </button>
            <button className="add-category-btn" onClick={handleAddCategory}>
              {!categoryInputIsTriggered ? "Add new category" : "< Close"}
            </button>
            {categoryInputIsTriggered ? (
              <div className="category-input-container">
                <input
                  type="text"
                  className="category-input"
                  placeholder="Type category here"
                  onChange={(e) => setCategoryName(e.target.value)}
                ></input>
                <span className="category-remover">&times;</span>
              </div>
            ) : (
              ""
            )}
            {categoryInputIsTriggered ? (
              <button
                className="submit-category-btn"
                onClick={() => postCategory()}
              >
                Submit
              </button>
            ) : (
              ""
            )}
            <Draggable defaultPosition={{ x: -320, y: 300 }}>
              <div>
                <ExpenseInput
                  trigger={inputIsTriggered}
                  onClick={() => postExpense()}
                  value={expenseName}
                  onChange={(e) => setExpenseName(e.target.value)}
                  close={() => setInputIsTriggered(false)}
                  onChangeAmount={(e) => setExpenseAmount(e.target.value)}
                  categories={categories}
                  onChangeCategory={(e) => setCategoryChosen(e.target.value)}
                />
              </div>
            </Draggable>
            <ExpensesContainer
              date={dateSelected}
              expensesContent={expensesContent}
              delete={(id, name) => triggerDeleteConfirmPopup(id, name)}
            />
            <Draggable defaultPosition={{ x: -320, y: 300 }}>
              <div>
                <DeleteConfirmPopup
                  trigger={deleteConfirmIsTriggered}
                  no={() => setDeleteConfirmIsTriggered(false)}
                  onClick={(id) => deleteExpense(id)}
                  deleteInfo={idAndNameToBeDeleted}
                />
              </div>
            </Draggable>
          </div>
        ) : (
          // Select date menu
          <div className="totalContainer-date-selector">
            <label className="selector-label">
              Select a date for viewing expenses
            </label>
            <input
              type="date"
              onChange={(e) => setDateSelected(e.target.value)}
              lang="en-GB"
            ></input>
            <button
              className="select-date-btn"
              onClick={dateSelected !== "" ? changeToExpensesMenu : undefined}
            >
              Select
            </button>
            <div className="graph-container">
              <Graph xAndY={xAxisAndYAxis} totalSpending={totalSpendingState} />
            </div>
          </div>
        )
      }
    </div>
  );
}
