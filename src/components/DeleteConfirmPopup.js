import './popup.css'

export default function DeleteConfirmPopup(props) {

    return props.trigger? (
        <div className="delete-confirm-popup">
            <p>Are you sure you wish to delete the "{props.deleteInfo.name}" expense?</p>
            <button onClick={() => props.onClick(props.deleteInfo.id)}>Yes</button>
            <button style={{marginLeft: "30px"}} onClick={props.no}>No</button>
        </div>
    ) : ("")
}