import * as d3 from "d3";
import { curveBasis } from "d3";
import { useEffect, useRef } from "react";
import { dateParser } from "../imported-functions/dateParser";

export function Graph(props) {
  const priceData = props.xAndY.map((obj) => {
    return obj.price;
  });

  const dateData = props.xAndY.map((obj) => {
    return dateParser(obj.date);
  });

  const maxPrice = Math.max(...priceData);

  console.log(dateData);

  const svgRef = useRef();

  useEffect(() => {
    // Setting up svg
    const w = 1300;
    const h = 500;
    const svg = d3
      .select(svgRef.current)
      .attr("width", w)
      .attr("height", h)
      .style("background", "#f0f8ff")
      .style("overflow", "visible");

    // Setting the x scale based on dates
    const xScale = d3
      .scaleTime()
      .domain([dateData[0], dateData[dateData.length - 1]])
      .range([0, w]);

    // Setting the y scale based on price
    const yScale = d3.scaleLinear().domain([0, maxPrice]).range([h, 0]);

    // Generating line
    const generateScaledLine = d3
      .line()
      .x((d, i) => xScale(dateData[i]))
      .y(yScale)
      .curve(d3.curveBasis);

    // Setting up Y and X axis
    const xAxis = d3.axisBottom(xScale);
    const yAxis = d3.axisLeft(yScale).ticks(5);

    // Appending x axis to svg
    svg.append("g").call(xAxis).attr("transform", `translate(0, ${h})`);

    // Appending Y axis to svg
    svg.append("g").call(yAxis);

    // Drawing line on svg
    svg
      .selectAll(".line")
      .data([priceData])
      .join("path")
      .attr("d", (d) => generateScaledLine(d))
      .attr("fill", "none")
      .attr("stroke", "black");
  }, [priceData, dateData, maxPrice]);

  return (
    <div>
      <svg ref={svgRef}></svg>
    </div>
  );
}
