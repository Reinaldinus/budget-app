import { nanoid } from "nanoid"

export default function ExpensesContainer(props) {

    

    const contentFilteredByDate = props.expensesContent.filter(expense => expense.date === props.date)

    const expensesMadeElements = contentFilteredByDate.map(expense => {
        
            return (
                <div 
                    id={expense.id}
                    className="expensesElement"
                    key={nanoid()}
                >
                    <p>{expense.name}</p>
                    <div className="expense-amount">
                        <p>€{expense.price}</p>
                    </div>
                    <button className="close-button" onClick={() => {props.delete(expense._id, expense.name)}}>
                        <span>&times;</span>
                    </button>
                </div>
            )
    })

    return (
        <div className="expensesContainer">
            {expensesMadeElements.length === 0 && <p>No expenses on this day.</p>}
            {expensesMadeElements}
        </div>
    )
}