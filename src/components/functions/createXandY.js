import { dateParser } from "../../imported-functions/dateParser";
import { fillInDates } from "../../imported-functions/fillInDates";
import { sortDate } from "../../imported-functions/sortDate";

export function createXandY(expensesContent) {

    let totalDayArray;
    let uniqueTotalDayArray;
    let totalDateObjects;
    let totalSpendingPerDay;
    let expenseAndDayObjects;
    totalDayArray = expensesContent.map(expense => {
        return (expense.date)
    })
  
    uniqueTotalDayArray = [...new Set(totalDayArray)]
  
    totalDateObjects = uniqueTotalDayArray.map(date => {
        return (dateParser(date))
    })
  
    totalDateObjects = fillInDates(sortDate(totalDateObjects))
    
    totalSpendingPerDay = totalDateObjects.map(date => {
        return({date: date, price: 0})
    })
  
    expenseAndDayObjects = expensesContent.map(expense => {
        return ({date: expense.date, price: parseInt(expense.price)})
    })
      
    const ExpensesPerDayWithoutDate = Object.values(expenseAndDayObjects.reduce((accumulator, obj) => {
        
        if (accumulator[obj.date] !== undefined)
          accumulator[obj.date] += (obj.price)
        else
          accumulator[obj.date] = (obj.price)
        
        return accumulator;
  
    }, {}))
  
    let ExpensesPerDayWithoutPrice = Object.values(expenseAndDayObjects.reduce((accumulator, obj) => {
        
        if (accumulator[obj.date] !== undefined)
          accumulator[obj.date] += (obj.date)
        else
          accumulator[obj.date] = (obj.date)
        
        return accumulator;
  
    }, {}))
  
    ExpensesPerDayWithoutPrice = ExpensesPerDayWithoutPrice.map(expense => {
        return (expense.slice(0, 10))
    })
  
    let merged = []
  
    for (let i = 0; i < ExpensesPerDayWithoutPrice.length; i++) {
        merged = merged.concat([{date: ExpensesPerDayWithoutPrice[i], price: ExpensesPerDayWithoutDate[i]}])
    }
      
    let mergedArray = [...totalSpendingPerDay, ...merged].reduce((acc, cur) => {
        const existingItem = acc.find(item => item.date === cur.date);
        if (existingItem) {
          existingItem.price += cur.price;
        } else {
          acc.push(cur);
        }
        return acc;
    }, []);
  
    return mergedArray
}