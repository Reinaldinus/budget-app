import { nanoid } from "nanoid"

export default function ExpenseInput(props) {

    const categoryOptions = props.categories.map(category => {
        return <option key={nanoid()}>{category.categoryName}</option>
    })

    return props.trigger? (
        <div className="expenseInput">
            <input 
                type="text"
                placeholder="Type expense here..."
                value={props.value}
                onChange={props.onChange}
            >
            </input>
            <div className="currency-wrap">
                <span className="currency-code">€</span>
                <input
                    type="number"
                    onKeyDown={(e) =>["e", "E", "+", "-", "."].includes(e.key) && e.preventDefault()}
                    onChange={props.onChangeAmount}
                >
                </input>
            </div>
            <select onChange={props.onChangeCategory}>
                <option disabled selected hidden>Select category</option>
                {categoryOptions}
            </select>
            <button className="add-expense-submit" onClick={props.onClick}>Add expense</button>
            <button className="close-button" onClick={props.close}>
                <span>&times;</span>
            </button>
        </div>
    ) : ("")
}