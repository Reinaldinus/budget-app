export default function DateSelected(props) {

    const currentDate = props.date

    return (
        <div className="date">
            <p className="dateSelected">Date selected: {currentDate}</p>
        </div>
    )
}