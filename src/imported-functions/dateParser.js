export function dateParser(date) {
    const match = /(\d{4})-(\d{2})-(\d{2})/.exec(date);

    if (match !== null) {
        const d = new Date(match[1], parseInt(match[2], 10) - 1, match[3]);
        if (isNaN(d.valueOf())) return;
        
        return d
      } else {
        return null;
      }
}