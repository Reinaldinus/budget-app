export function fillInDates(dates) {
    var minDate = new Date(dates[0]).getTime(),
    maxDate = new Date(dates[dates.length - 1]).getTime();
    
    var newDates = [],
    currentDate = minDate,
    dateObject;

    while (currentDate <= maxDate) {
        dateObject = new Date(currentDate);
        if (((dateObject.getMonth() + 1) < 10) && dateObject.getDate() < 10) {
            newDates.push(dateObject.getFullYear() + '-' + 0 + (dateObject.getMonth() + 1) + '-' + 0 + dateObject.getDate());
        }
        else if (((dateObject.getMonth() + 1) >= 10) && dateObject.getDate() < 10) {
            newDates.push(dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + 0 + dateObject.getDate());
        }
        else if (((dateObject.getMonth() + 1) < 10) && dateObject.getDate() >= 10) {
            newDates.push(dateObject.getFullYear() + '-' + 0 + (dateObject.getMonth() + 1) + '-' + dateObject.getDate());
        }
        else if (((dateObject.getMonth() + 1) >= 10) && dateObject.getDate() >= 10) {
            newDates.push(dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate());
        }
        
       currentDate += (24 * 60 * 60 * 1000); // add one day
    }
    return (newDates);
}