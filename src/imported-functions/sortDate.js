export function sortDate(dates) {
    return dates.sort((date1, date2) => date1 - date2);
}