const express = require("express");
const { ObjectId } = require("mongodb");
const { connectToDb, getDb } = require("./db");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

let db;

connectToDb((err) => {
  if (!err) {
    app.listen(5000, () => {
      console.log("Listening on port 5000...");
    });
    db = getDb();
  }
});

// Api for expenses

app.get("/expenses", (req, res) => {
  let items = [];

  db.collection("expenses")
    .find()
    .sort({ price: 1 })
    .forEach((item) => items.push(item))
    .then(() => {
      res.status(200).json(items);
    })
    .catch(() => {
      res.status(500).json({ error: "Could not fetch the items" });
    });
});

app.get("/expenses/:id", (req, res) => {
  db.collection("expenses")
    .findOne({ _id: new ObjectId(req.params.id) })
    .then((doc) => {
      res.status(200).json(doc);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not fetch the document" });
    });
});

app.post("/expenses", (req, res) => {
  const expense = req.body;

  db.collection("expenses")
    .insertOne(expense)
    .then((result) => {
      res.status(201).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not create document" });
    });
});

app.delete("/expenses/:id", (req, res) => {
  db.collection("expenses")
    .deleteOne({ _id: new ObjectId(req.params.id) })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not delete the document" });
    });
});

app.patch("/expenses/:id", (req, res) => {
  const updates = req.body;

  db.collection("expenses")
    .updateOne({ _id: new ObjectId(req.params.id) }, { $set: updates })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not update the document" });
    });
});

// Api for categories

app.get("/categories", (req, res) => {
  let items = [];

  db.collection("categories")
    .find()
    .sort({ price: 1 })
    .forEach((item) => items.push(item))
    .then(() => {
      res.status(200).json(items);
    })
    .catch(() => {
      res.status(500).json({ error: "Could not fetch the items" });
    });
});

app.post("/categories", (req, res) => {
  const expense = req.body;

  db.collection("categories")
    .insertOne(expense)
    .then((result) => {
      res.status(201).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not create document" });
    });
});

app.delete("/categories/:id", (req, res) => {
  db.collection("categories")
    .deleteOne({ _id: new ObjectId(req.params.id) })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not delete the document" });
    });
});

app.patch("/categories/:id", (req, res) => {
  const updates = req.body;

  db.collection("categories")
    .updateOne({ _id: new ObjectId(req.params.id) }, { $set: updates })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: "Could not update the document" });
    });
});
